import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import Sites from './components/Sites';
import Concurrency from './components/Concurrency';

function App() {

  const [search, storeSearch] = useState({ searchValue: '' });
  const [url, storeUrl] = useState(false);
  const [webDataApi, storeWebData] = useState([]);
  const { searchValue } = search;

  useEffect(() => {
    const executeRestApi = async () => {

      if (url) {
        const apiUrl = `http://localhost:4000/backoso-data/url?url=${searchValue}`;
        const response = await fetch(apiUrl);
        const webData = await response.json();
        storeWebData([...webDataApi, { webData }]);
        storeUrl(false);
      }
    }
    executeRestApi();

  }, [url]);

  return (
    <div className="container">
      <Header search={search} storeSearch={storeSearch} storeUrl={storeUrl} />
      <div className="row mt-3">
        <div className="col-lg-4 mb-3">
          <Concurrency webDataApi={webDataApi} />
        </div>
        <div className="col-lg-8 mb-3">
          <Sites webDataApi={webDataApi} />
        </div>
      </div>
    </div>
  );
}

export default App;
