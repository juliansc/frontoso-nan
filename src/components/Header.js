import React from 'react';
import Input from './Input';

function Header({search, storeSearch, storeUrl}) {
    return (
      <div className="container bg-dark border border-black rounded mt-4">
        <div className="p-4 text-white">
            <h4 className="text-primary text-center mb-3">Dependency analizer</h4>
            <Input search={search} storeSearch={storeSearch} storeUrl={storeUrl}/>
        </div>
      </div>
    );
  }
  
  export default Header;
  