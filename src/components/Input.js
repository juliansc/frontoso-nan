import React, {useState} from 'react';

function Input({ search, storeSearch, storeUrl}) {


    const[error, storeError] = useState(false)
    const { searchValue } = search;
    
    const handleChange = e => {
        storeSearch({
            ...searchValue,
            [e.target.name] : e.target.value
        });
    }

    const handleSubmit = e =>{
        e.preventDefault();
        if (searchValue.trim() === ''){
            storeError(true);
            return;
        }
        storeError(false);
        storeUrl(true);

    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="input-group mb-2">
                <input
                    type="text"
                    className="form-control border-0 text-white bg-main"
                    placeholder="Ingresar URL ej: http://google.com"
                    name="searchValue"
                    id="searchValue"
                    value={searchValue}
                    onChange={handleChange}
                />
                <div className="input-group-append">
                    <button type="submit" className="btn btn-primary">Analizar URL</button>
                </div>
            </div>
            { error ? <p className="text-danger m-0"><span className="badge badge-danger px-3">Woops!</span> <span className="small">Ingresar una URL</span></p> : null}
        </form>
    );
}

export default Input;