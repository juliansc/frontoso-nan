import React from 'react';
import Site from './Site';

function Sites({ webDataApi }) {
  if (webDataApi.length === 0) {
    return (
      <div className="row">
        <div className="col-lg-12 text-center">
          <div className="p-4 bg-dark rounded border border-black text-white">
            <p className="m-0 small">No hay datos disponibles</p>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="card-columns">
        <Site webDataApi={webDataApi} />
      </div>
    );
  }
}

export default Sites;
