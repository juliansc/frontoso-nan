import React, { Fragment } from 'react';
import uuid from 'react-uuid'

function Site({ webDataApi }) {


    return (

        <Fragment>
            {
                webDataApi.map((data) =>
                    <div key={uuid()} className="card bg-dark border border-black  mb-3">
                        <div className="card-body text-white p-0">
                            <div className="border-bottom border-black p-3">
                                <h4 className="card-title text-primary mb-0">{data.webData.title}</h4>
                            </div>
                            <div className="border-bottom border-black p-3">
                                {
                                    data.webData.dependencies ?
                                        data.webData.dependencies.map((dep) =>
                                            <p key={uuid()} className="small text-secondary mb-2">{dep}</p>
                                        )
                                        : null
                                }
                            </div>
                            <div className="p-3">
                                <p className="m-0">Bytes: {data.webData.bytes}</p>
                            </div>
                        </div>
                    </div>
                )
            }

        </Fragment>

    );
}

export default Site;
