import React from 'react';
import uuid from 'react-uuid'

function Concurrency({ webDataApi }) {

    if (webDataApi.length === 0) {
        return (
            <div className="row">
                <div className="col-lg-12 text-center">
                    <div className="p-4 bg-dark rounded border border-black text-white">
                        <p className="m-0 small">No hay datos disponibles</p>
                    </div>
                </div>
            </div>
        );
    } else {
        return (

            <div className="row">
                <div className="col-12">
                    <div className="rounded bg-dark border border-black text-white">
                        <h4 className="text-primary m-0 p-3 border-bottom border-black">All Dependencies</h4>
                        <ul className="list-group list-group-flush">
                            {
                                webDataApi.map((data) =>
                                    data.webData.dependencies ?
                                        data.webData.dependencies.map((dep) =>
                                            <li key={uuid()} className="list-group-item bg-transparent border-bottom border-black py-2">{dep}</li>
                                        )
                                        : null
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>

        );
    }

}

export default Concurrency;